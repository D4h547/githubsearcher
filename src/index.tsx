import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware, Store } from "redux"
import { Provider } from "react-redux"
import thunk from "redux-thunk"
import App from './frontend/App';
import './index.css';

// import App from "./frontend/App"
import reducer from "./frontend/components/reducer"

const store: Store<searchUserState, dataUserAction> & {
  dispatch: DispatchTypeUser
} = createStore(reducer, applyMiddleware(thunk))
ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
