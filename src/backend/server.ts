import * as redis from "redis";
import bodyParser from "body-parser";
import express from "express";
import axios from "axios";
import path from "path";

const client = redis.createClient(6379);
client.on("error", (error) => {
  console.error(error);
});

const port = process.env.PORT || 3001;
const isProduction = process.env.NODE_ENV === "production";
const expressApp = express();

expressApp.use(bodyParser.json(), bodyParser.urlencoded({ extended: true }));

expressApp.post("/api/search", (req, res) => {
  let textQuery: string = req.body.textQuery;
  var typeQuery: string = req.body.typeQuery;
  if ((typeQuery == "users" || typeQuery == "repositories") && textQuery) {
    try {
      let ct = textQuery + typeQuery;
      client.get(ct, async (err, data) => {
        if (data) {
          return res.status(200).send({
            error: false,
            message: `Data for ${textQuery} from the cache`,
            data: JSON.parse(data),
          });
        } else {
          try {
            const recipe = await axios.get(
              // `https://api.github.com/search/${typeConsul}?q=${consulta}`
              `https://api.github.com/${typeQuery}/${textQuery}`
            );
            client.setex(ct, 1020, JSON.stringify(recipe.data));
            return res.status(200).json({
              error: false,
              message: `Data for ${textQuery} from the server`,
              data: recipe.data,
            });
          } catch {
            return res.status(200).json({
              data: {
                error: true,
                message: "limite de consultas por minutos agotado",
              },
            });
          }
        }
      });
    } catch (error) {
      return res.status(200).json({
        data: {
          error: true,
          message: "error internal redis server",
        },
      });
    }
  } else {
    return res
      .status(404)
      .json({ success: false, msg: "data for is undefined" });
  }
});

expressApp.get("/api/clear-cache", (req, res) => {
  client.flushdb((err, succeeded) => {
    try {
      console.log(succeeded); // will be true if successfull
      return res.status(200).json({ success: true, msg: "cache is clear" });
    } catch {
      console.log(err);
    }
  });
});

if (isProduction) {
  const buildDir = path.join(process.cwd(), "build");
  expressApp.use(express.static(buildDir));
  expressApp.get("*", (req, res) => {
    res.send(path.join(buildDir, "index.html"));
  });
}

expressApp.listen(port, () => {
  console.log(`🚀 El servidor esta corriendo en puerto: ${port}`);
});
