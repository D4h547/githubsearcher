import * as React from "react";
import Grid from "@mui/material/Grid";
import Searcher from "./components/Search";
import GridSystem from "./components/GridSystem";
import { useSelector, shallowEqual } from "react-redux";
import "./css/App.css";

const App: React.FC = () => {
  const repos: searchUser = useSelector(
    (state: searchUserState) => state.data,
    shallowEqual
  );

  return (
    <div className="App">
      <Grid container justifyContent="center" alignItems="center">
        <Grid item xs={12}>
          <Searcher></Searcher>
        </Grid>
        <Grid item xs={8}>
          <GridSystem items={repos.searchForUser} />
        </Grid>
      </Grid>
    </div>
  );
};

export default App;
