import MuiAlert, { AlertProps } from "@mui/material/Alert";
import Snackbar from "@mui/material/Snackbar";
import Grid from "@mui/material/Grid";
import { SnackbarOrigin, SnackbarProvider } from "notistack";
import { addSearchUserData } from "./actionsCreator";
import { useDispatch } from "react-redux";
import { Dispatch } from "redux";
import { useState } from "react";
import React from "react";
import axios from "axios";
import "../css/Searcher.css";

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
  props,
  ref
) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});
type AlertColor = "success" | "info" | "warning" | "error";
const Searcher = () => {
  const dispatch: Dispatch<any> = useDispatch();
  const [query, setQuery] = useState("");
  const [typeQuery, setTypeQuery] = useState("users");
  const [variant, setVariant] = useState<AlertColor>("warning" as AlertColor);
  const [msgError, setMsgError] = useState("");
  const [activeAlertWarning, setActiveAlertWarning] = useState(false);
  const saveDataSearchUser = React.useCallback(
    (data: searchUser) => dispatch(addSearchUserData(data)),
    [dispatch]
  );
  const positions: SnackbarOrigin = {
    vertical: "bottom",
    horizontal: "right",
  };

  const getDataSearch = async (newQuery: string) => {
    if (newQuery.length > 3) {
      try {
        const json = JSON.stringify({
          textQuery: newQuery,
          typeQuery: typeQuery,
        });
        const res = await axios.post("/api/search", json, {
          headers: {
            "Content-Type": "application/json",
          },
        });
        // console.log(res.data.data)
        if (!res.data.data.error) {
          saveDataSearchUser({
            searchForUser: [res.data.data],
            incomplete_results: true,
            total_count: 0,
          });
        } else {
          setActiveAlertWarning(true);
          setVariant("warning");
          setMsgError(res.data.data.message);
          saveDataSearchUser({
            searchForUser: [],
            incomplete_results: true,
            total_count: 0,
          });
        }
      } catch (error) {
        console.log(error);
      }
    } else {
      saveDataSearchUser({
        searchForUser: [],
        incomplete_results: true,
        total_count: 0,
      });
    }
  };

  const getValueInputSearch = (e: React.FormEvent<HTMLInputElement>) => {
    const newQuery = e.currentTarget.value;
    getDataSearch(e.currentTarget.value);
    setQuery(newQuery);
  };

  const getValueSelectSearch = (e: React.FormEvent<HTMLSelectElement>) => {
    const newTypeConsul = e.currentTarget.value;
    getDataSearch(query);
    setTypeQuery(newTypeConsul);
  };

  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === "clickaway") {
      return;
    }
    setActiveAlertWarning(false);
  };

  function handleClick() {
    const res: any = axios
      .get("/api/clear-cache", {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then((res) => {
        setVariant("success");
        setActiveAlertWarning(true);
        setMsgError(res.data.msg);
        // console.log(res);
      });
  }

  return (
    <Grid container spacing={1} justifyContent="center" alignItems="center">
      <Grid item xs={12} sm={12} md={12}>
        <div className="header-app">
          <h1 className="header-title">GitHub Searcher</h1>
          <div className="container-search">
            <Grid
              container
              spacing={1}
              justifyContent="center"
              alignItems="center"
            >
              <Grid item xs={12} sm={6} md={5}>
                <input
                  type="text"
                  placeholder="Search..."
                  className="header-input-search"
                  onChange={getValueInputSearch}
                  id="icon"
                />
              </Grid>
              <Grid item xs={12} sm={6} md={5}>
                <select
                  className="header-search-type"
                  name=""
                  id=""
                  onChange={getValueSelectSearch}
                >
                  <option className="search-type-option" value="users">
                    Users
                  </option>
                  <option className="search-type-option" value="repositories">
                    Repositories
                  </option>
                </select>
              </Grid>
            </Grid>
            <button className="btn-clear" onClick={handleClick}>
              clear cache
            </button>
            <div></div>
          </div>
        </div>
      </Grid>
      <SnackbarProvider maxSnack={3}>
        <Snackbar
          open={activeAlertWarning}
          autoHideDuration={6000}
          onClose={handleClose}
          anchorOrigin={positions}
        >
          <Alert
            onClose={handleClose}
            severity={variant}
            sx={{ width: "100%" }}
          >
            {msgError}
          </Alert>
        </Snackbar>
      </SnackbarProvider>
    </Grid>
  );
};

export default Searcher;
