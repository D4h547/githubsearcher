import "../css/CardRepo.css";
type cardUserProps = {
  cardUser: userCard;
};

export default function BasicCard(props: cardUserProps) {
  return (
    <div className="card-app">
      <div className="card-container">
        <div className="card-image">
          <img src={props.cardUser.avatar_url} alt="" />
        </div>
        <div>
          <h3 className="card-title">{props.cardUser.name ? props.cardUser.name: props.cardUser.login}</h3>
        <h4 className="card-subtitle">repos: {props.cardUser.public_repos}</h4>
        </div>
      </div>
      <div className="card-text">
        <p className="card-p"><span>company:</span> {props.cardUser.company ? props.cardUser.company : 'none'}</p>
          <p  className="card-p"><span>followers:</span> {props.cardUser.followers}</p>
        <p className="card-p"><span>type:</span> {props.cardUser.type}</p>
        <p className="card-p"><a href={props.cardUser.blog} target="_blank">link blog</a></p>

      </div>
      <div className="card-actions">
          <a href={props.cardUser.html_url} target="_blank">
            to git
        </a>
      </div>
    </div>
  );
}
