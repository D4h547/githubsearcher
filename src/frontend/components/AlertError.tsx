import React from 'react';
import Button from '@mui/material/Button';
import { SnackbarProvider, VariantType, useSnackbar, SnackbarOrigin } from 'notistack';

function MyApp() {
  const { enqueueSnackbar } = useSnackbar();

  const positions: SnackbarOrigin = {
    vertical: 'bottom',
    horizontal: 'right'
  }

  const handleClickVariant = (variant: VariantType, anchorOrigin: SnackbarOrigin) => () => {
    enqueueSnackbar('This is a success message!', { variant, anchorOrigin });
  };

  return (
    <React.Fragment>
      <Button onClick={handleClickVariant('warning', positions)}>Show success snackbar</Button>
    </React.Fragment>
  );
}

export default function IntegrationNotistack() {
  return (
    <SnackbarProvider maxSnack={3} >
      <MyApp />
    </SnackbarProvider>
  );
}