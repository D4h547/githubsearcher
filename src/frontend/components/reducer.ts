import * as actionTypes from "./actions";

const initialState: searchUserState = {
  data: {
    searchForUser: [],
    incomplete_results: false,
    total_count: 0,
  },
};

const reducer = (
  state: searchUserState = initialState,
  action: dataUserAction
): searchUserState => {
  switch (action.type) {
    case actionTypes.ADD_DATA:
      const newData: searchUser = {
        searchForUser: action.searchUserData.searchForUser,
        incomplete_results: action.searchUserData.incomplete_results,
        total_count: action.searchUserData.total_count,
      };
      return {
        ...state,
        data: newData,
      };
    // case actionTypes.REMOVE_DATA:
    //   const updatedData: Idata = state.data.filter(
    //     (data) => data.items !== action.data.items
    //   );
    //   return {
    //     ...state,
    //     data: updatedData,
    //   };
  }
  return state;
};

export default reducer;
