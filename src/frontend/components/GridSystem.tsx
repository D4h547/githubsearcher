import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import CardSearchUser from "./CardSearchUser";
import CardSleep from "./CardSleep";

type reposProps = {
  items: userCard[];
};
const GridSystem = (props: reposProps) => {
  if (props.items.length !== 0) {
    return (
      <Box sx={{ flexGrow: 1, paddingTop: 4 }}>
        <Grid container spacing={1} justifyContent="center" alignItems="center">
          {props.items.map((item) => {
            return (
              <Grid item xs={4}>
                <CardSearchUser cardUser={item} />
              </Grid>
            );
          })}
        </Grid>
      </Box>
    );
  } else {
    return (
      <Box sx={{ flexGrow: 1, paddingTop: 4 }}>
        <Grid container spacing={1} justifyContent="center" alignItems="center">
          <Grid item xs={10} sm={6} md={4}>
            <CardSleep></CardSleep>
          </Grid>
          <Grid item xs={10} sm={6} md={4}>
            <CardSleep></CardSleep>
          </Grid>
          <Grid item xs={10} sm={6} md={4}>
            <CardSleep></CardSleep>
          </Grid>
          <Grid item xs={10} sm={6} md={4}>
            <CardSleep></CardSleep>
          </Grid>
          <Grid item xs={10} sm={6} md={4}>
            <CardSleep></CardSleep>
          </Grid>
          <Grid item xs={10} sm={6} md={4}>
            <CardSleep></CardSleep>
          </Grid>
        </Grid>
      </Box>
    );
  }
};
export default GridSystem;
