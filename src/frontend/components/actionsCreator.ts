import * as actionTypes from "./actions";

export function addSearchUserData(searchUserData: searchUser) {
  const action: dataUserAction = {
    type: actionTypes.ADD_DATA,
    searchUserData,
  };

  return simulateHttpRequest(action);
}

export function removeSearchUserData(searchUserData: searchUser) {
  const action: dataUserAction = {
    type: actionTypes.REMOVE_DATA,
    searchUserData,
  };
  return simulateHttpRequest(action);
}

export function addSearchReposData(searchReposData: searchRepos) {
  const action: dataReposAction = {
    type: actionTypes.ADD_DATA,
    searchReposData,
  };

  return simulateHttpRequest2(action);
}
export function simulateHttpRequest(action: dataUserAction) {
  console.log(action);
  return (dispatch: DispatchTypeUser) => {
    dispatch(action);
  };
}

export function simulateHttpRequest2(action: dataReposAction) {
  console.log(action);
  return (dispatch: DisspatchTypeRepos) => {
    dispatch(action);
  };
}
